@extends('layouts.Master')


<div class="page-title-box">
     
                                          
</div><!--end page title box-->

    <div class="page-content">
        <div class="container-fluid">      
            <div class="row">
                <div class="col-12">                                                
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-right">
                                <a class="btn btn-success" href="{{route('create')}}">
                                <i class="fa fa-plus"></i>
                                <span>Create New Post</span>
                                </a>
                            </div>
                            </div>
                    </div>
                           <table id="datatable2" class="table dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                $i = 1;
                                @endphp
                               @foreach($user as $show)
                               
                                        <tr>
                                            <td>{{ $i }}</td>
                                                @php
                                                $i++;
                                                @endphp
                                            
                                            <td>{{$show->name}}</td>
                                            <td>{{$show->email}}</td>
                                            <td>{{$show->role}}</td>
                                            <td>
                <a class="btn btn-primary" href="{{ route('edit',$show->id)}}">
                  <i class="far fa-edit"></i>
                 Edit</a>
                 
                  <a class="btn btn-danger delete_conform" href="{{ route('delete',$show->id) }}">
                  <i class="far fa-trash-alt"></i> 
                  Delete

                   
                                    </td>
                                </tr>
                                 @endforeach
                                </tbody>
                            </table>        
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
    </div>
      
<script type="text/javascript">
    $(document).ready(function() {
      var table = $('#datatable2').DataTable({
      });
</script>

