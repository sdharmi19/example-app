@extends('layouts.Master')


<div class="page-title-box">
     
                                          
</div><!--end page title box-->

    <div class="page-content">
        <div class="container-fluid">      
            <div class="row">
                <div class="col-12">                                                
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            
                            </div>
                    </div>
                            <div class="modal-body bg-white">
                <form method="post" action="{{ route ('update',$edit->id)}}">
                    @csrf
                    <div class="col-md-12 form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name"  placeholder="Enter Name" name="name" value="{{$edit->name}}">
                       
                    </div>
                     <div class="col-md-12 form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email"  placeholder="Enter Email" name="email" value="{{$edit->email}}">
                       
                    </div>

                     <div class="col-md-12 form-group">
                            <label for="email">Roles</label>   
                            <select name="role" id="role" class="form-control" required>
                                <option value="admin">Admin</option>
                                <option value="employee">Employee</option>
 
                                </select>
                            </div>
                    <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Update
                                </button>
                               
                            </div>
                </form>  
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
    </div>
       
