@extends('layouts.Master')
<div class="page-title-box">
    
</div>
<div class="page-content">
        <div class="container-fluid">      
            <div class="row">
                <div class="col-12">                                                
                    <div class="card">
                        <div class="card-body">
                            <div class="row ">
                                <div class="col-10">
                                                       
                                </div>
                               
                            </div>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                 
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                              @php
                                $i = 1;
                                @endphp
                               <tr>
                                <td>{{ $i }}</td>
                                @php
                                $i++;
                                @endphp
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td> <a class="btn btn-danger" href="{{route('useredit',$user->id)}}">
                  <span class="glyphicon glyphicon-trash"></span> 
                  Edit</td>
                                </tr>
                               
                                </tbody>
                            </table>        
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
    </div>
     
<script type="text/javascript">
    $(document).ready(function() {
      var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          // buttons: ['copy', 'excel', 'pdf']
          //colvis
      });

</script>

