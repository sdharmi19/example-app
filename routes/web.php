<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return redirect('user');
   // return view('dashboard');
})->name('dashboard');
Route::get('/user','App\Http\Controllers\HomeController@index');

Route::middleware(['auth','admin'])->group(function(){
	
	
		Route::get('/admin', 'App\Http\Controllers\AdminController@index')->name('admin_dashboard');
		Route::get('/create','App\Http\Controllers\AdminController@create')->name('create');
		Route::any('/add','App\Http\Controllers\AdminController@add')->name('add');

		Route::get('/delete/{id}','App\Http\Controllers\AdminController@delete')->name('delete');
		Route::get('/edit/{id}','App\Http\Controllers\AdminController@edit')->name('edit');	
		Route::post('/update/{id}','App\Http\Controllers\AdminController@update')->name('update');	
	});



Route::middleware(['auth','employee'])->group(function(){
	Route::get('/emp', 'App\Http\Controllers\EmployeeController@index')->name('employee_dashboard');
	Route::get('/useredit/{id}','App\Http\Controllers\EmployeeController@useredit')->name('useredit');	
		Route::any('/userupdate/{id}','App\Http\Controllers\EmployeeController@userupdate')->name('userupdate');
	
});



