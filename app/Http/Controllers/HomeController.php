<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class HomeController extends Controller
{
    public function index()
    {
    	//$user = Auth::user();
    	//dd($user);
    	$role = Auth::User()->role;
        
        if($role=='admin')
        {
           
            return redirect()->route('admin_dashboard'); 
        }
        else 
        {
            
          return redirect()->route('employee_dashboard'); 
        }
    }
}
