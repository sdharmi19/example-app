<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Session;
use Validator;

class AdminController extends Controller
{
    //
   

    public function index()
    {
    	$user = User::all();
        return view('admin.home',compact('user'));
    }
    public function create(Request $request)
    {
    	return view('admin.add');
    }
    public function add(Request $request)
    {
         $validatedData = Validator::make($request->all(),
        [
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role' => ['required'],
        ]);
    	$user = new User;
    	$user->name = $request->name;
    	$user->email = $request->email;
        $user->role = $request->role;
    	$user->save();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Added successfully.'); 
    	return redirect()->route('admin_dashboard'); 
    }
    public function delete($id)
    { 
        $userDelete = User::find($id);
        
        $userDelete->delete();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Deleted successfully.');
        return redirect()->route('admin_dashboard');
    }
    public function edit(Request $request,$id)
    {
        $edit = User::find($id);

        return view('admin.edit',compact('edit'));
    }
    public function update(Request $request,$id)
    {
        $update = User::find($id);
        $update->name = $request->name;
        $update->email = $request->email;
        $update->role =  $request->role;
        $update->save();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Updated successfully.'); 
        return redirect()->route('admin_dashboard'); 
    }

}
