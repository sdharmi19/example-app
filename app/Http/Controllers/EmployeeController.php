<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Session;

class EmployeeController extends Controller
{
    //
     

    public function index(Request $request)
    {
    	$user = Auth::user();
    	//dd($user);	
        return view('employee.home',compact('user'));
    }
     public function useredit(Request $request,$id)
    {
        $useredit = User::find($id);

        return view('employee.edit',compact('useredit'));
    }
    public function userupdate(Request $request,$id)
    {
        $update = User::find($id);
        $update->name = $request->name;
        $update->email = $request->email;
        $update->save();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Updated successfully.'); 

        return redirect()->route('employee_dashboard'); 
    }
}
