<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\User;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::User()->role;
        
        if($role == 'admin')
        {
            return $next($request);            
        }
        else
        {
             return redirect('home');
        }
    }
}
